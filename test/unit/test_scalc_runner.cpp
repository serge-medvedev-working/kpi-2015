#include "scalc_runner.hpp"
#include "test_utils.hpp"

#include <turtle/mock.hpp>

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( TS_scalc_runner )

MOCK_BASE_CLASS( mock_scalc, scalc )
{
    MOCK_CONST_METHOD( add, 1, int64_t ( const std::string & ) )
};

static auto create_mock_scalc()
{
    return std::make_shared< mock_scalc >();
}

class dummy_scalc_view : public scalc_view
{
public: // functions
    virtual void display( const std::string & ) {}
};

static scalc_view_ref create_dummy_scalc_view()
{
    return std::make_shared< dummy_scalc_view >();
}

BOOST_AUTO_TEST_CASE( TransmitsCorrectlyRarsedToSCalc )
{
    auto view = create_dummy_scalc_view();
    auto scalc = create_mock_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;
    std::string app_name( "scalc" ),
        args( "2,4,6,8,10" );

    cmd_line
        .add_token( app_name )
        .add_token( args )
    ;

    MOCK_EXPECT( scalc->add )
        .once()
        .with( args )
        .returns( 0 )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_CASE( TransmitsSCalcResultToDisplay )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_mock_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;
    std::string app_name( "scalc" ),
        args( "" );

    cmd_line
        .add_token( app_name )
        .add_token( args )
    ;

    MOCK_EXPECT( scalc->add )
        .once()
        .returns( 666 )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "666" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_SUITE_END()

