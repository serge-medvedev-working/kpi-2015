#include "cmd_line_builder.hpp"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( TS_cmd_line_builder )

BOOST_AUTO_TEST_CASE( StoresAndReturnsAllTheArgsCorrectly )
{
    cmd_line_builder cmd_line;
    std::string app_name( "app_name" ),
        arg1( "--arg1 value1" ),
        arg2( "--arg2=value2" );
    
    cmd_line
        .add_token( app_name )
        .add_token( arg1 )
        .add_token( arg2 )
    ;

    BOOST_REQUIRE_EQUAL( cmd_line.argc(), 3 );

    char ** argv( cmd_line.argv() );

    BOOST_REQUIRE_EQUAL( argv[ 0 ], app_name );
    BOOST_REQUIRE_EQUAL( argv[ 1 ], arg1 );
    BOOST_REQUIRE_EQUAL( argv[ 2 ], arg2 );
    BOOST_REQUIRE( argv[ 3 ] == nullptr );
}

BOOST_AUTO_TEST_SUITE_END()

