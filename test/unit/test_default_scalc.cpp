#include "default_scalc.hpp"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( TS_default_scalc )

BOOST_AUTO_TEST_CASE( ReturnsZeroOnEmptyInput )
{
    default_scalc sc;

    auto result = sc.add( "" );

    BOOST_REQUIRE_EQUAL( result, 0 );
}

BOOST_AUTO_TEST_CASE( ReturnsArgValueIfThereIsOnlyOneArg )
{
    default_scalc sc;

    auto result = sc.add( "256" );

    BOOST_REQUIRE_EQUAL( result, 256 );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfCommaSeparatedArgs )
{
    default_scalc sc;

    auto result = sc.add( "2,2" );

    BOOST_REQUIRE_EQUAL( result, 4 );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfLongCommaSeparatedSequence )
{
    default_scalc sc;

    auto result = sc.add( "0,1,1,2,3,5,8,13,21" );

    BOOST_REQUIRE_EQUAL( result, 54 );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfArgsSeparatedWithColon )
{
    default_scalc sc;

    auto result = sc.add( "//:\n3:3:3" );

    BOOST_REQUIRE_EQUAL( result, 9 );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfArgsSeparatedWithThreeEqualsSigns )
{
    default_scalc sc;

    auto result = sc.add( "//[===]\n2===4===8===16" );

    BOOST_REQUIRE_EQUAL( result, 30 );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfArgsSeparatedWithTwoCustomDelimiters )
{
    default_scalc sc;

    auto result = sc.add( "//[<>][$$]\n40<>0$$1<>1" );

    BOOST_REQUIRE_EQUAL( result, 42 );
}

BOOST_AUTO_TEST_CASE( ThrowsOnNegativeArgs )
{
    default_scalc sc;

    BOOST_REQUIRE_THROW(
        sc.add( "10,20,-30,40" ),
        std::runtime_error
    );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfArgsIgnoringThoseGreaterThan1000 )
{
    default_scalc sc;

    auto result = sc.add( "//#\n1#1001#2#1002#3" );

    BOOST_REQUIRE_EQUAL( result, 6 );
}

BOOST_AUTO_TEST_SUITE_END()

