#ifndef __MOCKS_HPP__
#define __MOCKS_HPP__

#include "scalc_view.hpp"

#include <turtle/mock.hpp>

#include <string>
#include <memory>

MOCK_BASE_CLASS( mock_scalc_view, scalc_view )
{
    MOCK_METHOD( display, 1, void ( const std::string & ) )
};

static std::shared_ptr< mock_scalc_view > create_mock_scalc_view()
{
    return std::make_shared< mock_scalc_view >();
}

#endif // __MOCKS_HPP__

