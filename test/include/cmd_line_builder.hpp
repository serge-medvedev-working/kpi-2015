#ifndef __CMD_LINE_BUILDER_HPP__
#define __CMD_LINE_BUILDER_HPP__

#include <string>
#include <vector>

class cmd_line_builder
{
    std::vector< std::vector< char > > _buffers;
    std::vector< char * > _pointers;

public: // functions
    cmd_line_builder & add_token( const std::string & token )
    {
        std::vector< char > buffer( token.cbegin(), token.cend() );

        buffer.push_back( '\0' );

        _buffers.push_back( std::move( buffer ) );

        return *this;
    }

    int argc() const
    {
        return _buffers.size();
    }

    char ** argv()
    {
        _pointers.clear();

        for ( auto & buffer : _buffers )
        {
            _pointers.push_back( buffer.data() );
        }

        _pointers.push_back( nullptr );

        return _pointers.data();
    }
};

#endif // __CMD_LINE_BUILDER_HPP__

