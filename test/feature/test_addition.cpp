#include "scalc_runner.hpp"
#include "default_scalc.hpp"
#include "test_utils.hpp"

#include <turtle/mock.hpp>

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE( TS_addition )

static scalc_ref create_default_scalc()
{
    return std::make_shared< default_scalc >();
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfLongSequenceSeparatedWithComma )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_default_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;

    cmd_line
        .add_token( "scalc" )
        .add_token( "1,2,3,4,5,6,7,8,9,0" )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "45" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfLongSequenceSeparatedWithCommasAndEOLs )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_default_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;

    cmd_line
        .add_token( "scalc" )
        .add_token( "2\n4,6\n8,10\n12,14" )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "56" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfLongSequenceSeparatedWithCustomDelimiter )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_default_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;

    cmd_line
        .add_token( "scalc" )
        .add_token( "//;\n1;3;5;7;9;11;13" )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "49" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfLongSequenceSeparatedWithLongCustomDelimiter )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_default_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;

    cmd_line
        .add_token( "scalc" )
        .add_token( "//[***]\n0***1***1***2***3***5***8" )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "20" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfLongSequenceSeparatedWithManyCustomDelimiters )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_default_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;

    cmd_line
        .add_token( "scalc" )
        .add_token( "//[***][||][%]\n10***8||6%4***2||0" )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "30" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_CASE( DispaysErrorMessageIfThereAreNegativeArgs )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_default_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;

    cmd_line
        .add_token( "scalc" )
        .add_token( "1,2,3,4,-5,6,7,8,9" )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "#Error" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_CASE( ReturnsSumOfLongSequenceIgnoringArgsGreaterThan1000 )
{
    auto view = create_mock_scalc_view();
    auto scalc = create_default_scalc();
    scalc_runner sr( view, scalc );
    cmd_line_builder cmd_line;

    cmd_line
        .add_token( "scalc" )
        .add_token( "1,10,100,1000,10000" )
    ;

    MOCK_EXPECT( view->display )
        .once()
        .with( "1111" )
    ;

    sr.run( cmd_line.argc(), cmd_line.argv() );
}

BOOST_AUTO_TEST_SUITE_END()

