#ifndef __DEFAULT_SCALC_HPP__
#define __DEFAULT_SCALC_HPP__

#include "scalc.hpp"

class default_scalc : public scalc
{
public: // functions
    virtual int64_t add( const std::string & args ) const;

private: // functions
    int sum_args_separated_with_comma( const std::string & args ) const;
    std::string replace_all_delimiters_with_comma( const std::string & args ) const;
    void throw_on_negative_args( int arg_value, const char * message ) const;
    int zero_if_greater_than( int arg_value, int limit ) const;
};

#endif // __DEFAULT_SCALC_HPP__

