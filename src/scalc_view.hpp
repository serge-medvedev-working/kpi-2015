#ifndef __SCALC_VIEW_HPP__
#define __SCALC_VIEW_HPP__

#include <string>
#include <memory>

class scalc_view
{
public: // functions
    virtual ~scalc_view() = default;

    virtual void display( const std::string & text ) = 0;
};

using scalc_view_ref = std::shared_ptr< scalc_view >;

#endif // __SCALC_VIEW_HPP__

