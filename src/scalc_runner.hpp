#ifndef __SCALC_RUNNER_HPP__
#define __SCALC_RUNNER_HPP__

#include "scalc_view.hpp"
#include "scalc.hpp"

class scalc_runner
{
    scalc_view_ref _view;
    scalc_ref _scalc;

public: // functions
    scalc_runner( scalc_view_ref view, scalc_ref scalc );

    void run( int argc, char ** argv );
};

#endif // __SCALC_RUNNER_HPP__

