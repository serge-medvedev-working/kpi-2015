#include "default_scalc.hpp"

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>

#include <sstream>
#include <list>

int64_t default_scalc::add( const std::string & args ) const
{
    if ( args.empty() )
    {
        return 0;
    }

    auto custom_args = replace_all_delimiters_with_comma( args );

    return sum_args_separated_with_comma( custom_args );
}

int default_scalc::sum_args_separated_with_comma( const std::string & args ) const
{
    std::istringstream iss( args );
    int64_t arg_value( 0 ), result( 0 );

    while ( iss >> arg_value )
    {
        throw_on_negative_args( arg_value, "negative args are not allowed" );

        arg_value = zero_if_greater_than( arg_value, 1000 );

        result += arg_value;

        if ( iss.peek() == ',' )
        {
            iss.ignore();
        }
    }

    return result;
}

std::string default_scalc::replace_all_delimiters_with_comma( const std::string & args ) const
{
    boost::regex initial_regex( "^(//(?<delims>([^\\d]+))\n)(?<args>(.+))$" );
    boost::smatch matches;

    if ( ! boost::regex_match( args, matches, initial_regex ) )
    {
        return args;
    }

    std::string all_delimiters( matches[ "delims" ] );
    boost::regex rexp(
        "((?<delim>([^\\d\\[\\]]))|(\\[(?<delim>([^\\d\\[\\]]{2,}))\\]))"
    );
    boost::sregex_iterator ri_begin( all_delimiters.cbegin(), all_delimiters.cend(), rexp ), ri_end;
    std::list< std::string > custom_delimiters;

    std::for_each( ri_begin, ri_end, [ &custom_delimiters ] ( auto & match )
    {
        custom_delimiters.push_back( match[ "delim" ] );
    } );

    std::string only_args( matches[ "args" ] );

    for ( auto & custom_delimiter : custom_delimiters )
    {
        boost::replace_all( only_args, custom_delimiter, "," );
    }

    return only_args;
}

void default_scalc::throw_on_negative_args( int arg_value, const char * message ) const
{
    if ( arg_value < 0 )
    {
        throw std::runtime_error( message );
    }
}

int default_scalc::zero_if_greater_than( int arg_value, int limit ) const
{
    return ( arg_value > limit ? 0 : arg_value );
}

