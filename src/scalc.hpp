#ifndef __SCALC_HPP__
#define __SCALC_HPP__

#include <string>
#include <cstdint>
#include <memory>

class scalc
{
public: // functions
    virtual ~scalc() = default;

    virtual int64_t add( const std::string & ) const = 0;
};

using scalc_ref = std::shared_ptr< scalc >;

#endif // __SCALC_HPP__

