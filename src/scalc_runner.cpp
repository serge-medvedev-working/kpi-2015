#include "scalc_runner.hpp"

#include <string>

scalc_runner::scalc_runner( scalc_view_ref view, scalc_ref scalc )
    : _view( view )
    , _scalc( scalc )
{
}

void scalc_runner::run( int argc, char ** argv )
{
    try
    {
        std::string args( argv[ 1 ] );
        auto result = _scalc->add( args );
        
        _view->display( std::to_string( result ) );
    }
    catch ( const std::runtime_error & )
    {
        _view->display( "#Error" );
    }
}

